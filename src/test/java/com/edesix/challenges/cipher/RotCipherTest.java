package com.edesix.challenges.cipher;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;


public class RotCipherTest {

    private String asciiAlphabet;
    private String asciiNumeric;

    @Before
    public void setUp() {
        this.asciiAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        this.asciiNumeric = "0123456789";
    }

    @Test
    public void testRot13RotateAlphabet() {
        String rotated = RotCipher.rot13(this.asciiAlphabet);

        assertEquals("NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm", rotated);
        // ROT13 is a special case where the rotation is the inverse of itself.
        assertEquals(this.asciiAlphabet, RotCipher.rot13(rotated));
    }

    @Test
    public void testRot5RotateNumeric() {
        String rotated = RotCipher.rot5(this.asciiNumeric);

        assertEquals("5678901234", rotated);
        // ROT5 is a special case where the rotation is the inverse of itself.
        assertEquals(this.asciiNumeric, RotCipher.rot5(rotated));
    }

    @Test
    public void testRot18RotateAlphabetNumeric() {
        String msg = this.asciiAlphabet + this.asciiNumeric;
        String expected = "NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm5678901234";
        String rotated = RotCipher.rot18(msg);

        assertEquals(expected, rotated);
        assertEquals(msg, RotCipher.rot18(rotated));
    }

}
