package com.edesix.challenges.cipher;

/**
 * A generic cipher for rotating ASCII alphabetic characters by n places.
 */
public class RotCipher {

    /**
     * A generic helper function for rotating an ASCII characters between a set range of characters. If {@code c} is not
     * within the desired character range then it will not be rotated.
     *
     * @param c         character to rotate
     * @param startC    first character in ASCII range
     * @param endC      last character in ASCII range
     * @param rotations number of rotations
     * @return {@code c} with the ASCII characters rotated by {@code rotations}.
     */
    private static char rotateChar(char c, char startC, char endC, int rotations) {
        if (startC > endC) {
            throw new IllegalArgumentException("The startC character cannot be larger than the endC");
        }

        if (c >= startC && c <= endC) {
            // get the distance between the characters
            int range = endC - startC + 1;
            c = (char) ((((c - startC) + rotations) % range) + startC);
        }
        return c;
    }

    /**
     * Rotates the ASCII alphabetic characters (A-Za-z) by 13 places.
     *
     * @param input value to be rotated
     * @return {@code input} with ASCII alphabetic characters rotated by 13 places.
     */
    public static String rot13(String input) {
        char[] charArr = new char[input.length()];
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            // rotate the character by 13 if it falls within the char range of a-z or A-Z.
            c = rotateChar(c, 'a', 'z', 13);
            c = rotateChar(c, 'A', 'Z', 13);
            charArr[i] = c;
        }
        return String.valueOf(charArr);
    }

    /**
     * Rotates the ASCII numeric characters (0-9) by 5 places.
     *
     * @param input value to be rotated
     * @return {@code input} with ASCII numeric characters rotated by 5 places.
     */
    public static String rot5(String input) {
        char[] charArr = new char[input.length()];
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            // rotate the character by 5 if it falls within the char range of 0-9.
            c = rotateChar(c, '0', '9', 5);
            charArr[i] = c;
        }
        return String.valueOf(charArr);
    }


    /**
     * Rotates the ASCII alphabetic characters (A-Za-z) by 13 places and the ASCII numeric
     * characters (0-9) by 5 places.
     *
     * @param input value to be rotated
     * @return {@code input} with ASCII alphabetic characters rotated by 13 places and ASCII
     *         numeric characters rotated by 5 places.
     */
    public static String rot18(String input) {
        char[] charArr = new char[input.length()];
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            // rotate the character by 13 if it falls within the char range of a-z, A-Z or 0-9.
            c = rotateChar(c, 'a', 'z', 13);
            c = rotateChar(c, 'A', 'Z', 13);
            c = rotateChar(c, '0', '9', 5);
            charArr[i] = c;
        }
        return String.valueOf(charArr);
    }


    public static void main(String[] args) {
        // Message from the specification that needs to be decrypted.
        String msg = "Ceboyrz fbyivat vf 35 creprag bs gur wbo";

        System.out.println("Original Text:");
        System.out.println(msg);
        System.out.println();

        System.out.println("ROT5:");
        System.out.println(RotCipher.rot5(msg));
        System.out.println();
        // prints "Ceboyrz fbyivat vf 80 creprag bs gur wbo"

        System.out.println("ROT13:");
        System.out.println(RotCipher.rot13(msg));
        System.out.println();
        // prints "Problem solving is 35 percent of the job"

        System.out.println("ROT5 + ROT13:");
        System.out.println(RotCipher.rot5(RotCipher.rot13(msg)));
        System.out.println();
        // prints "Problem solving is 80 percent of the job"

        // ROT18 is more efficient as it only needs to loop once rather than twice with ROT5 and ROT13.
        System.out.println("ROT18:");
        System.out.println(RotCipher.rot18(msg));
        System.out.println();
        // prints "Problem solving is 80 percent of the job"

    }

}
