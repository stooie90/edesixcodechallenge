# Edesix Code Challenge

## Specification

The following text has been encoded using ROT13 and ROT5:

"Ceboyrz fbyivat vf 35 creprag bs gur wbo"

Write a small Java program to decrypt this message, and send us the answer along with your code.

You can find out more about ROT13 and ROT5 here: http://en.wikipedia.org/wiki/ROT13


Please note: There are several approaches you could take to this. Write at least one and describe at least one other,
considering the pros and cons of each - for example performance, scalability, ease of understanding etc.

## How to Build & Run

The project can be built using the gradle wrapper included in the root of the project directory. To build & run the
the tests, enter the command:

```
./gradlew clean build
```

This will output all the compiled files into a '<project_root>/build' directory and will also create a JAR file that
can be used to run the project in '<project_root>/build/libs'.

You can now run the code using the command which will output the decryption stages to the terminal:

```
java -jar build/libs/EdesixCodeChallenge-0.1.0.jar
```

## Secondary Solution

A second solution could be to have a map of characters to their expected output. I.e.

```
String original = "abc...xyz";
String rot13chars = "nop...klm";
```

As we loop the characters we can do an `int i = original.indexOf(c)` lookup and get the character from 
`rot13chars.charAt(i)` at the same index. This is readable and simple to understand but it's not as flexible as my 
current solution for adapting the code to work with other ROTn functions. My solution would also allow you to change
the range of characters in which to rotate and will work at a speed of O(n). A simple implementation of `indexOf()`
would loop through each character before finding the index which would cause O(n*r) where n is the length of the input
 and r is thr character range (26 for ROT13).
    